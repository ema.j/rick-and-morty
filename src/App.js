import "./App.scss";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import HomePage from "./Pages/Homepage";
import CardDetails from "./Pages/CardDetails";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/:id" element={<CardDetails />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
