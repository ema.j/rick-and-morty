import React from "react";
import { Link } from "react-router-dom";
import "./Cards.scss";

const Cards = ({ results, page }) => {
  let display;

  if (results) {
    display = results.map((x) => {
      let { id, name, image, species } = x;
      return (
        <Link to={`${page}${id}`} key={id} className="card">
          <img src={image} className="card-img" alt="rick-and-morty" />
          <div className="card-content">
            <h3 className="card-title">{name}</h3>
            <div>
              <p className="specie-title">Specie</p>
              <p className="specie">{species}</p>
            </div>
          </div>
        </Link>
      );
    });
  } else {
    display = (
      <h1 className="not-found">
        That character is not in this universe.{" "}
        <span className="search-again">Maybe search again?</span>
      </h1>
    );
  }

  return <>{display}</>;
};

export default Cards;
