import React from "react";
import "./Subtitle.scss";

const Subtitle = ({ title }) => {
  return (
    <div className="subtitle">
      <div className="subtitle-line"></div>
      <h1 className="subtitle-txt">{title}</h1>
      <div className="subtitle-line"></div>
    </div>
  );
};

export default Subtitle;
