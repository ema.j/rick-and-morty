import React from "react";
// import ReactPaginate from "react-paginate";
import "./Pagination.scss";

const Pagination = ({ pageNumber, setPageNumber }) => {
  let prevPage = () => {
    if (pageNumber === 1) return;
    setPageNumber((page) => page - 1);
  };

  let nextPage = () => {
    setPageNumber((page) => page + 1);
  };

  return (
    <div className="buttons">
      <button onClick={prevPage} className="btn">
        Prev
      </button>
      <button onClick={nextPage} className="btn">
        Next
      </button>
    </div>

    // <ReactPaginate pageCount={info.pages}/>
  );
};

export default Pagination;
