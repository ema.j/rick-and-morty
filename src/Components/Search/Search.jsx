import React from "react";
import "./Search.scss";

const Search = ({ setSearch, setPageNumber }) => {
  function handleChange(e) {
    setPageNumber(1);
    setSearch(e.target.value);
  }

  return (
    <input
      className="search-bar"
      onChange={handleChange}
      type="text"
      placeholder="Search for Characters"
    />
  );
};

export default Search;
