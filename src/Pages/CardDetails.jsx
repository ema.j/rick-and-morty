import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Title from "../assets/img/title.png";
import "./CardDetails.scss";

const CardDetails = () => {
  let { id } = useParams();
  let [fetchData, updateFetchData] = useState([]);
  let { name, image, status, gender, species, location } = fetchData;
  let api = `https://rickandmortyapi.com/api/character/${id}`;
  useEffect(() => {
    (async function () {
      let data = await fetch(api).then((res) => res.json());
      updateFetchData(data);
    })();
  }, [api]);

  return (
    <>
      <div className="details-bg">
        <div className="title">
          <img src={Title} className="title-img" alt="rick-and-morty" />
        </div>
        <div className="details">
          <img src={image} className="image" alt="rick-and-morty" />
          <div className="content">
            <h2 className="character-title">{name}</h2>
            <div>
              <span className="characteristic-title">Specie: </span>
              <span className="characteristic">{species}</span>
            </div>
            <div>
              <span className="characteristic-title">Status: </span>
              <span className="characteristic">{status}</span>
            </div>
            <div>
              <span className="characteristic-title">Gender: </span>
              <span className="characteristic">{gender}</span>
            </div>
            <div>
              <span className="characteristic-title">Location: </span>
              <span className="characteristic">{location?.name}</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CardDetails;
