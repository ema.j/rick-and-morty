import React, { useState, useEffect } from "react";
import Title from "../assets/img/title.png";
import Search from "../Components/Search/Search";
import Subtitle from "../Components/Subtitle/Subtitle";
import Cards from "../Components/Cards/Cards";
import Pagination from "../Components/Pagination/Pagination";
import "./HomePage.scss";

export default function HomePage() {
  let [pageNumber, setPageNumber] = useState(1);
  let [search, setSearch] = useState("");
  let [fetchData, updateFetchData] = useState([]);
  let { results } = fetchData;

  let api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${search}`;

  useEffect(() => {
    (async function () {
      let data = await fetch(api).then((res) => res.json());
      updateFetchData(data);
    })();
  }, [api]);

  return (
    <>
      <div className="home-bg">
        <div className="container">
          <div className="title">
            <img src={Title} className="title-img" alt="rick-and-morty" />
          </div>

          <Search setPageNumber={setPageNumber} setSearch={setSearch} />
          <Subtitle title="List of characters" />

          <div className="cards-container">
            <Cards page="/" results={results} />
          </div>

          <Pagination pageNumber={pageNumber} setPageNumber={setPageNumber} />
        </div>
      </div>
    </>
  );
}
